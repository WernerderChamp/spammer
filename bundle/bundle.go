package bundle

import (
	"time"

	"math/rand"

	"github.com/iotaledger/iota.go/bundle"
	"github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/kerl"
	"github.com/iotaledger/iota.go/signing"
	"github.com/iotaledger/iota.go/trinary"
)

var sec1Fragments = []trinary.Trytes{consts.NullSignatureMessageFragmentTrytes}
var sec2Fragments = []trinary.Trytes{consts.NullSignatureMessageFragmentTrytes, consts.NullSignatureMessageFragmentTrytes}
var sec3Fragments = []trinary.Trytes{consts.NullSignatureMessageFragmentTrytes, consts.NullSignatureMessageFragmentTrytes, consts.NullSignatureMessageFragmentTrytes}
var bundle1 = preCreateBundle1()
var bundle2 = preCreateBundle2()

// I don't care about the M bug, might consider that in a future version => much faster without
func FinalizeInsecure(template bundleTemplate) (bundle.Bundle, error) {

	//resolve template into bundle
	var b bundle.Bundle

	for i := 0; i < len(template.inputs); i++ {
		b = bundle.AddEntry(b, template.inputs[i])
	}

	for i := 0; i < len(template.outputs); i++ {
		b = bundle.AddEntry(b, template.outputs[i])
	}

	var bLength = len(b)

	var valueTrits = make([]trinary.Trits, bLength)
	var timestampTrits = make([]trinary.Trits, bLength)
	var currentIndexTrits = make([]trinary.Trits, bLength)
	var obsoleteTagTrits = make([]trinary.Trits, bLength)
	var lastIndexTrits = trinary.PadTrits(trinary.IntToTrits(int64(b[0].LastIndex)), 27)

	for i := range b {
		valueTrits[i] = trinary.PadTrits(trinary.IntToTrits(b[i].Value), 81)
		timestampTrits[i] = trinary.PadTrits(trinary.IntToTrits(int64(b[i].Timestamp)), 27)
		currentIndexTrits[i] = trinary.PadTrits(trinary.IntToTrits(int64(b[i].CurrentIndex)), 27)
		obsoleteTagTrits[i] = trinary.PadTrits(trinary.MustTrytesToTrits(b[i].ObsoleteTag), 81)
	}

	var bundleHash trinary.Hash

	k := kerl.NewKerl()

	for i := range b {
		relevantTritsForBundleHash := trinary.MustTrytesToTrits(
			b[i].Address +
				trinary.MustTritsToTrytes(valueTrits[i]) +
				trinary.MustTritsToTrytes(obsoleteTagTrits[i]) +
				trinary.MustTritsToTrytes(timestampTrits[i]) +
				trinary.MustTritsToTrytes(currentIndexTrits[i]) +
				trinary.MustTritsToTrytes(lastIndexTrits),
		)
		k.Absorb(relevantTritsForBundleHash)
	}

	bundleHashTrits, err := k.Squeeze(consts.HashTrinarySize)
	if err != nil {
		return nil, err
	}
	bundleHash = trinary.MustTritsToTrytes(bundleHashTrits)

	if len(template.inputs) != 0 {
		//if we have inputs, we gotta sign them
		//normalize bundle hash
		normalizedBundleHash := signing.NormalizedBundleHash(bundleHash)
		bundleIndex := 0
		for i := range template.signatureTemplates {
			localTemplate := &template.signatureTemplates[i]
			var subseed, err = signing.Subseed(localTemplate.seed, localTemplate.index)
			if err != nil {
				return b, err
			}
			var key, err2 = signing.Key(subseed, localTemplate.secLevel)
			if err2 != nil {
				return b, err2
			}
			//signing
			for x := 0; x < int(localTemplate.secLevel); x++ {
				var signatureTrits, err = signing.SignatureFragment(normalizedBundleHash[0+27*x:27*(x+1)], key[0+6561*x:6561*(x+1)])
				if err != nil {
					return b, err
				}
				b[bundleIndex].SignatureMessageFragment = trinary.MustTritsToTrytes(signatureTrits)
				bundleIndex++
			}
		}
	}

	// set the computed bundle hash on each tx in the bundle
	for i := range b {
		tx := &b[i]
		tx.Bundle = bundleHash
	}

	return b, nil
}

func CreateBundle(address string, msg string, tagSubstring string, txCount int, additionalMesssage ...string) (bundle.Bundle, error) {
	var number = rand.Float64()
	if number >= 0.5 {
		return bundle1, nil
	} else {
		return bundle2, nil
	}
}

/*func CreateBundleOld(address string, msg string, tagSubstring string, txCount int, additionalMesssage ...string) (bundle.Bundle, error) {

	tag, err := trinary.NewTrytes(tagSubstring + utils.IntegerToAscii(txCount))
	if err != nil {
		return nil, fmt.Errorf("NewTrytes: %v", err.Error())
	}
	now := time.Now()

	messageString := msg + fmt.Sprintf("\nCount: %06d", txCount)
	messageString += fmt.Sprintf("\nTimestamp: %s", now.Format(time.RFC3339))
	if len(additionalMesssage) > 0 {
		messageString = fmt.Sprintf("%v\n%v", messageString, additionalMesssage[0])
	}

	message, err := converter.ASCIIToTrytes(messageString)
	if err != nil {
		return nil, fmt.Errorf("ASCIIToTrytes: %v", err.Error())
	}

	timestamp := uint64(now.UnixNano() / int64(time.Millisecond))

	var template bundleTemplate

	outEntry := bundle.BundleEntry{
		Address:                   address,
		Value:                     0,
		Tag:                       tag,
		Timestamp:                 timestamp,
		Length:                    uint64(1),
		SignatureMessageFragments: []trinary.Trytes{trinary.Pad(message, consts.SignatureMessageFragmentSizeInTrytes)},
	}
	template.AddEntry(outEntry)
	sigTemplate := inputSignatureTemplate{
		seed:     "JDXUFG99COIPTIXYFMN9VYWFUUXGTO9FPHOSE9LNMXGXWTBMXQULJBRYPBY9VOYQBSKLZVSGNZVG9YELE",
		index:    0,
		secLevel: 2,
	}
	template.AddTransaction("U9RFDUACLOFEVYFQOSZMHYDHFBGXPPO9INCNYOCSOIPQKOXFBMJAIUVSUIPAFNSJJOZ9NXKTFGZFBJVZZ", "U9RFDUACLOFEVYFQOSZMHYDHFBGXPPO9INCNYOCSOIPQKOXFBMJAIUVSUIPAFNSJJOZ9NXKTFGZFBJVZZ", 10000, tag, timestamp, sigTemplate, message)
	var b bundle.Bundle

	// finalize bundle by adding the bundle hash
	b, err = FinalizeInsecure(template)
	if err != nil {
		return nil, fmt.Errorf("Bundle.Finalize: %v", err.Error())
	}

	return b, nil
}*/

func preCreateBundle1() bundle.Bundle {
	var template bundleTemplate

	now := time.Now()
	timestamp := uint64(now.UnixNano() / int64(time.Millisecond))

	sigTemplate := inputSignatureTemplate{
		seed:     "JDXUFG99COIPTIXYFMN9VYWFUUXGTO9FPHOSE9LNMXGXWTBMXQULJBRYPBY9VOYQBSKLZVSGNZVG9YELE", //E
		index:    0,
		secLevel: 2,
	}
	template.AddTransaction("U9RFDUACLOFEVYFQOSZMHYDHFBGXPPO9INCNYOCSOIPQKOXFBMJAIUVSUIPAFNSJJOZ9NXKTFGZFBJVZZ", "DILJBVXPTWEYTNDEUVL9IRBSAIXU9KONNYBIVEHFRGDWGQVTCSCMHTFFEPTBSXCIXSMBKFUFKQCFYYUIC", 1, "TANGLEKIT9SCAMMER9999999999", timestamp, sigTemplate, "SENDING999IOTA")

	// finalize bundle by adding the bundle hash
	var b, err = FinalizeInsecure(template)
	if err != nil {
		panic("failed to create default spam bundle, exiting")
	}

	return b
}
func preCreateBundle2() bundle.Bundle {
	var template bundleTemplate

	now := time.Now()
	timestamp := uint64(now.UnixNano() / int64(time.Millisecond))

	sigTemplate := inputSignatureTemplate{
		seed:     "XRLODK9ZQXEV9VGCDZMMYOKVACKKYYRBZUZSWRH9YXOQTQTMJEKZZVTOJNBLOWTBU9YNECH9K9JQRKHYC", //C
		index:    0,
		secLevel: 2,
	}
	template.AddTransaction("DILJBVXPTWEYTNDEUVL9IRBSAIXU9KONNYBIVEHFRGDWGQVTCSCMHTFFEPTBSXCIXSMBKFUFKQCFYYUIC", "U9RFDUACLOFEVYFQOSZMHYDHFBGXPPO9INCNYOCSOIPQKOXFBMJAIUVSUIPAFNSJJOZ9NXKTFGZFBJVZZ", 1, "TANGLEKIT9SCAMMER9999999999", timestamp, sigTemplate, "BACK999AND999FORTH")

	// finalize bundle by adding the bundle hash
	var b, err = FinalizeInsecure(template)
	if err != nil {
		panic("failed to create default spam bundle, exiting")
	}

	return b
}

func (t *bundleTemplate) AddEntry(entry bundle.BundleEntry) {
	t.outputs = append(t.outputs, entry)
}

func (t *bundleTemplate) AddTransaction(fromAddress trinary.Hash, toAddress trinary.Hash, value int64, tag trinary.Trytes, timestamp uint64, sigTemplate inputSignatureTemplate, message string) {
	inputEntry := bundle.BundleEntry{
		Address:   fromAddress,
		Value:     (value * -1),
		Tag:       tag,
		Timestamp: timestamp,
		Length:    uint64(sigTemplate.secLevel),
	}
	switch sigTemplate.secLevel {
	case 1:
		inputEntry.SignatureMessageFragments = sec1Fragments
	case 2:
		inputEntry.SignatureMessageFragments = sec2Fragments
	case 3:
		inputEntry.SignatureMessageFragments = sec3Fragments
	}
	t.inputs = append(t.inputs, inputEntry)
	t.signatureTemplates = append(t.signatureTemplates, sigTemplate)
	outputEntry := bundle.BundleEntry{
		Address:                   toAddress,
		Value:                     value,
		Tag:                       tag,
		Timestamp:                 timestamp,
		Length:                    uint64(1),
		SignatureMessageFragments: []trinary.Trytes{trinary.Pad(message, consts.SignatureMessageFragmentSizeInTrytes)},
	}
	t.outputs = append(t.outputs, outputEntry)
	if t.maxSecLevel < sigTemplate.secLevel {
		t.maxSecLevel = sigTemplate.secLevel
	}

}

type inputSignatureTemplate struct {
	seed     string
	index    uint64
	secLevel consts.SecurityLevel
}

type bundleTemplate struct {
	inputs             []bundle.BundleEntry
	outputs            []bundle.BundleEntry
	signatureTemplates []inputSignatureTemplate
	maxSecLevel        consts.SecurityLevel
}
