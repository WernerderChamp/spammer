package utils

import (
	"container/heap"
	"sync"
	"time"
)

type TimeHeap struct {
	Lock  *sync.Mutex
	times []time.Time
}

func (h TimeHeap) Len() int           { return len(h.times) }
func (h TimeHeap) Less(i, j int) bool { return h.times[i].Before(h.times[j]) }
func (h TimeHeap) Swap(i, j int)      { h.times[i], h.times[j] = h.times[j], h.times[i] }

func (h *TimeHeap) GetCountTTL(timeBefore time.Time) int {
	h.Lock.Lock()
	defer h.Lock.Unlock()

	lenHeap := len((*h).times)
	if lenHeap > 0 {
		for i := 0; i < lenHeap; i++ {
			oldest := heap.Pop(h)
			if timeBefore.Before(oldest.(time.Time)) {
				heap.Push(h, oldest)
				break
			}
		}
	}

	return len((*h).times)
}

func (h *TimeHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	(*h).times = append((*h).times, x.(time.Time))
}

func (h *TimeHeap) Pop() interface{} {
	old := (*h).times
	n := len(old)
	x := old[n-1]
	(*h).times = old[0 : n-1]
	return x
}
