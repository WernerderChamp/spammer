package utils

func IntegerToAscii(number int) string {
	alphabet := "9ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	result := ""
	for index := 0; index < 7; index++ {
		pos := number % 27
		number /= 27
		result = string(alphabet[pos]) + result
	}
	return result
}
