# TangleKit Spammer

Spam the IOTA tangle with fast powsrv.io proof-of-work

## Preparation

1. Install Go (Golang) (at least 1.12.x) on your System
2. If you want to use [powsrv.io](https://powsrv.io) for the PoW, register or log in to the [powsrv.io dashboard](https://my.powsrv.io) and copy your API Key

## Set up 
1. Clone the spammer or download it
2. Change to the spammer directory
3. Run `go get -v -d ./...` (optional)
4. Edit the config.json
   - Insert your powsrv.io API Key (e.g.: `"powSrvAPIKey": "YOUR9API9KEY",`) or set `powLocal` to `true` if you want to do the PoW on your own.
   
   - Add as many Nodes as you know to the Node list:
     ```
     "nodeList": [
      "http://first-node.tld:80",
      "https://second-node.tld:443",
      "http://123.456.789.111:14265",
      .
      .
      .
      "http://last-node.tld:24234"
     ],
     ```
5. Run the spammer: `./run_spammer.sh`


## Problems or Bugs? 
Feel free to open an issue 
