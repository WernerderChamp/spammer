package main

import (
	"container/heap"
	"container/list"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	"github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/pow"
	"github.com/iotaledger/iota.go/transaction"
	"github.com/iotaledger/iota.go/trinary"

	batchedCurl "github.com/iotadevelopment/go/packages/curl"

	powsrvio "gitlab.com/powsrv.io/go/client"

	"gitlab.com/tanglekit/go/logs"
	"gitlab.com/tanglekit/go/nodelist"

	spammerBundle "gitlab.com/tanglekit/spammer/bundle"
	"gitlab.com/tanglekit/spammer/config"
	"gitlab.com/tanglekit/spammer/utils"
)

var (
	milestoneCheckInterval = time.Duration(1) * time.Second

	// Globals
	deathWaitGroup = &sync.WaitGroup{}
	curlP81Batch   = batchedCurl.CURLP81
)

type Spammer struct {
	quitSignal        bool
	signalChannelQuit chan struct{}

	powFuncLocal          pow.ProofOfWorkFunc
	powSrvClient          *powsrvio.PowClient
	powSrvClientLock      *sync.RWMutex
	powSrvClientErrSignal chan struct{}

	powSemaphore       chan int
	prepareTxSemaphore chan int

	txsChannel           chan []trinary.Trytes
	tipsGTTAChannel      chan *TipsGTTA
	rateLimitPowChannel  chan struct{}
	rateLimitGTTAChannel chan struct{}

	txCount       int
	attachedCount int
	powedCount    int
	gTTACount     int

	avgAttached float32
	avgPowed    float32
	avgGTTA     float32

	avgAttachedLock *sync.Mutex
	avgPowedLock    *sync.Mutex
	avgGTTALock     *sync.Mutex

	timeHeapAttached    *utils.TimeHeap
	timeHeapPowed       *utils.TimeHeap
	timeListGTTA        *list.List
	timeDurationSumGTTA int64

	printAvgTicker       *time.Ticker
	rateLimitTicker      *time.Ticker
	milestoneCheckTicker *time.Ticker

	nodeList        *nodelist.ApiNodeList
	latestMilestone *nodelist.Milestone
	tagSubstring    string
}

type TipsGTTA struct {
	Trunk    trinary.Hash
	Branch   trinary.Hash
	NodeInfo string
}

// TransactionHash makes a transaction hash from the given transaction.
func TransactionHash(t *transaction.Transaction) trinary.Hash {
	respChan := curlP81Batch.Hash(trinary.MustTrytesToTrits(transaction.MustTransactionToTrytes(t)))
	respTrits := <-respChan
	return trinary.MustTritsToTrytes(respTrits)
}

func (s *Spammer) InitSpammer() {
	var err error

	s.quitSignal = false
	s.signalChannelQuit = make(chan struct{})

	config.LoadFlags()

	cfg := config.AppConfig

	logs.Log.Infof("Spamming with MWM %d", cfg.GetInt("mwm"))

	s.powSemaphore = make(chan int, cfg.GetInt("powWorkersCount"))
	s.prepareTxSemaphore = make(chan int, cfg.GetInt("powWorkersCount"))

	if cfg.GetBool("powLocal") {
		_, s.powFuncLocal = pow.GetFastestProofOfWorkImpl()
	} else {
		if cfg.GetString("powSrvAPIKey") == "" {
			logs.Log.Fatal("No powsrv.io API key given!")
		}
		s.powSrvClientLock = &sync.RWMutex{}
		s.InitPowClient()
	}

	s.txsChannel = make(chan []trinary.Trytes, 10000)
	s.tipsGTTAChannel = make(chan *TipsGTTA, cfg.GetInt("tipsBufferSize"))

	if cfg.GetInt64("powRateLimit") != 0 {
		var channelSize int64 = 1000
		if cfg.GetInt64("powRateLimit")*2 > channelSize {
			channelSize = cfg.GetInt64("powRateLimit") * 2
		}
		s.rateLimitPowChannel = make(chan struct{}, channelSize)
		s.rateLimitGTTAChannel = make(chan struct{}, channelSize)

		for cnt := 0; cnt < cfg.GetInt("powRateLimit")*2; cnt++ {
			// Prefill channel that gTTA can start from beginning
			s.rateLimitGTTAChannel <- struct{}{}
		}

		s.rateLimitTicker = time.NewTicker(time.Duration(int64(time.Second) / cfg.GetInt64("powRateLimit")))

		go func() {
			for {
				select {
				case <-s.signalChannelQuit:
					return
				case <-s.rateLimitTicker.C:
					select {
					case s.rateLimitPowChannel <- struct{}{}:
					default:
						// Channel full
					}

					select {
					case s.rateLimitGTTAChannel <- struct{}{}:
					default:
						// Channel full
					}
				}
			}
		}()
	}

	s.avgAttachedLock = &sync.Mutex{}
	s.avgPowedLock = &sync.Mutex{}
	s.avgGTTALock = &sync.Mutex{}

	s.nodeList = &nodelist.ApiNodeList{}
	nodeTimeout := time.Duration(cfg.GetInt64("apiNodeTimeout")) * time.Second

	nodelistFromConfig := cfg.GetStringSlice("nodeList")
	if len(nodelistFromConfig) == 0 {
		logs.Log.Fatal("No IOTA fullnodes found in configuration!")
	}

	err = s.nodeList.Init(nodelistFromConfig, nodeTimeout, cfg.GetStringSlice("validNodeVersions"), cfg.GetInt64("milestoneTolerance"), true)
	if err != nil {
		logs.Log.Fatal(err)
	}

	if s.nodeList.GetHealthyNodeCount() == 0 {
		logs.Log.Fatal("No healty IOTA fullnode found!")
	}

	s.latestMilestone = s.nodeList.GetLatestMilestone()

	s.milestoneCheckTicker = time.NewTicker(milestoneCheckInterval)

	go func() {

		// Thread that handles new milestones
		fn := func(apiNode *nodelist.ApiNode) error {
			nodeInfoResp, err := apiNode.API.GetNodeInfo()
			if err != nil {
				return err
			}

			if (s.latestMilestone == nil) || (s.latestMilestone.Index < nodeInfoResp.LatestMilestoneIndex) {
				milestoneTxs, err := apiNode.API.GetTransactionObjects(nodeInfoResp.LatestMilestone)
				if err == nil {
					s.latestMilestone = &nodelist.Milestone{Hash: &nodeInfoResp.LatestMilestone, Index: nodeInfoResp.LatestMilestoneIndex, Timestamp: milestoneTxs[0].Timestamp}
					logs.Log.Infof("New Milestone (%v): %v", s.latestMilestone.Index, *s.latestMilestone.Hash)
				}
			}

			return nil
		}

		for range s.milestoneCheckTicker.C {
			signalChanTimeout := make(chan bool, 1)
			err = s.nodeList.ExecuteFunc(signalChanTimeout, s.signalChannelQuit, fn, 0)
			for err != nil {
				err = s.nodeList.ExecuteFunc(signalChanTimeout, s.signalChannelQuit, fn, 0)
			}
		}
	}()

	s.tagSubstring = string([]rune(cfg.GetString("tag"))[:20])

	s.txCount = 0
	s.attachedCount = 0
	s.powedCount = 0
	s.gTTACount = 0

	s.avgAttached = 0.0
	s.avgPowed = 0.0
	s.avgGTTA = 0.0

	s.timeHeapAttached = &utils.TimeHeap{Lock: &sync.Mutex{}}
	s.timeHeapPowed = &utils.TimeHeap{Lock: &sync.Mutex{}}
	s.timeListGTTA = list.New()

	heap.Init(s.timeHeapAttached)
	heap.Init(s.timeHeapPowed)

	s.printAvgTicker = time.NewTicker(time.Second)

	go func() {
		for range s.printAvgTicker.C {
			ta := time.Now()

			s.avgAttachedLock.Lock()
			s.avgAttached = float32(s.timeHeapAttached.GetCountTTL(ta.Add(-10*time.Second))) / float32(10)
			s.avgAttachedLock.Unlock()

			s.avgPowedLock.Lock()
			s.avgPowed = float32(s.timeHeapPowed.GetCountTTL(ta.Add(-10*time.Second))) / float32(10)
			s.avgPowedLock.Unlock()

			timeSinceLastMilestone := time.Duration(0)
			if s.latestMilestone != nil {
				timeSinceLastMilestone = time.Now().UTC().Sub(time.Unix(int64(s.latestMilestone.Timestamp), 0)).Truncate(time.Second)
			}
			logs.Log.Infof("HealthyNodes: %v/%v, TxsChannel: %v, Attached (Avg): %d (%0.2f), Powed (Avg): %d (%0.2f), gTTA (Avg): %d (%0.2fs), Latest MS age: %v", s.nodeList.GetHealthyNodeCount(), s.nodeList.GetTotalNodeCount(), len(s.txsChannel), s.attachedCount, s.avgAttached, s.powedCount, s.avgPowed, s.gTTACount, s.avgGTTA, timeSinceLastMilestone)
		}
	}()
}

func (s *Spammer) Close() {

	s.quitSignal = true
	close(s.signalChannelQuit)

	if s.rateLimitTicker != nil {
		s.rateLimitTicker.Stop()
	}

	if s.milestoneCheckTicker != nil {
		s.milestoneCheckTicker.Stop()
	}

	if s.printAvgTicker != nil {
		s.printAvgTicker.Stop()
	}

	if s.rateLimitPowChannel != nil {
		close(s.rateLimitPowChannel)
	}
	if s.rateLimitGTTAChannel != nil {
		close(s.rateLimitGTTAChannel)
	}

	close(s.tipsGTTAChannel)
	close(s.txsChannel)

	if !config.AppConfig.GetBool("powLocal") {
		s.powSrvClient.Close()
	}
}

func (s *Spammer) RunSpammer() {
	s.StartSendThread()

	cfg := config.AppConfig

	var fnGTTA func(apiNode *nodelist.ApiNode) error

	// Create Function pointer for the spammer
	fnGTTA = func(apiNode *nodelist.ApiNode) error {
		if s.quitSignal {
			return nil
		}

		timeGTTA := time.Now()
		transactionsToApprove, err := apiNode.API.GetTransactionsToApprove(uint64(cfg.GetInt64("depth")))
		if err != nil {
			return fmt.Errorf("GetTransactionsToApprove: %v", err.Error())
		}
		durationGTTA := time.Since(timeGTTA)

		if s.quitSignal {
			return nil
		}

		nodeInfo := "Node: "
		if cfg.GetBool("addNodeAddressInMessage") {
			nodeInfo = fmt.Sprintf("%v%v (v%v)", nodeInfo, apiNode.Address, apiNode.Version)
		} else {
			nodeInfo = fmt.Sprintf("%vv%v", nodeInfo, apiNode.Version)
		}
		nodeInfo = fmt.Sprintf("%v, gTTA took %v (depth=%v)", nodeInfo, durationGTTA.Truncate(time.Millisecond), cfg.GetInt64("depth"))

		s.tipsGTTAChannel <- &TipsGTTA{Trunk: transactionsToApprove.TrunkTransaction, Branch: transactionsToApprove.BranchTransaction, NodeInfo: nodeInfo}
		s.AddAvgGTTA(1, durationGTTA.Nanoseconds()/1000000)

		if cfg.GetBool("printGTTADuration") {
			logs.Log.Infof("GetTransactionsToApprove took %v", durationGTTA.Truncate(time.Millisecond))
		}

		return nil
	}

	// Create Function pointer for the spammer
	fnPrepareTx := func() error {
		defer func() { <-s.prepareTxSemaphore }() // read once from the channel => semaphore release

		if s.quitSignal {
			return nil
		}

		var err error
		var b bundle.Bundle
		transactionsToApprove := &api.TransactionsToApprove{}
		var nodeInfo *string

		tipsGTTA := <-s.tipsGTTAChannel
		if tipsGTTA != nil {
			transactionsToApprove.TrunkTransaction = tipsGTTA.Trunk
			transactionsToApprove.BranchTransaction = tipsGTTA.Branch
			nodeInfo = &tipsGTTA.NodeInfo
		}

		if s.quitSignal {
			return nil
		}

		s.txCount++
		b, err = spammerBundle.CreateBundle(cfg.GetString("address")[:consts.HashTrytesSize], cfg.GetString("message"), s.tagSubstring, s.txCount, *nodeInfo)

		if err != nil {
			logs.Log.Fatalf("createBundle: %v", err.Error())
		}

		if cfg.GetInt64("powRateLimit") != 0 {
			<-s.rateLimitPowChannel
		}

		err = s.doPow(b, transactionsToApprove.TrunkTransaction, transactionsToApprove.BranchTransaction, cfg.GetInt("mwm"))
		for err != nil {
			logs.Log.Errorf("doPow: %v", err.Error())
			if s.quitSignal {
				return nil
			}
			err = s.doPow(b, transactionsToApprove.TrunkTransaction, transactionsToApprove.BranchTransaction, cfg.GetInt("mwm"))
		}

		txs, err := transaction.TransactionsToTrytes(b)
		if err != nil {
			logs.Log.Fatalf("transaction.TransactionsToTrytes: %v", err.Error())
		}

		if !s.quitSignal {
			s.txsChannel <- txs
		}

		return nil
	}

	go func() {
		for !s.quitSignal {

			for len(s.txsChannel) > 100 {
				// As long as the tx channel is full, don't block the nodes with gTTA
				time.Sleep(500 * time.Millisecond)
			}

			// TODO: If someday gTTA is faster than PoW, drop old/lazy tips from the channel
			if cfg.GetInt64("powRateLimit") != 0 {
				<-s.rateLimitGTTAChannel
			}

			signalChanTimeout := make(chan bool, 1)
			err := s.nodeList.ExecuteFunc(signalChanTimeout, s.signalChannelQuit, fnGTTA, 0)
			if err != nil {
				logs.Log.Warningf("ExecuteFunc: %v", err.Error())
			}
		}
	}()

	for !s.quitSignal {
		s.prepareTxSemaphore <- 1 // channel is not full => semaphore aquire
		go fnPrepareTx()
	}
}

func (s *Spammer) AddAvgTPS(newTx int) {

	s.attachedCount += newTx

	ta := time.Now()

	s.avgAttachedLock.Lock()
	for i := 0; i < newTx; i++ {
		heap.Push(s.timeHeapAttached, ta)
	}
	s.avgAttachedLock.Unlock()
}

func (s *Spammer) AddAvgPOW(newTx int) {

	s.powedCount += newTx

	ta := time.Now()

	s.avgPowedLock.Lock()
	for i := 0; i < newTx; i++ {
		heap.Push(s.timeHeapPowed, ta)
	}
	s.avgPowedLock.Unlock()
}

func (s *Spammer) AddAvgGTTA(newGTTA int, gTTADuration int64) {

	s.avgGTTALock.Lock()

	s.gTTACount += newGTTA
	for i := 0; i < newGTTA; i++ {
		s.timeListGTTA.PushBack(gTTADuration)
		s.timeDurationSumGTTA += gTTADuration
	}

	// Remove old values
	for s.timeListGTTA.Len() > 100 {
		e := s.timeListGTTA.Front()
		s.timeListGTTA.Remove(e) // Dequeue first element
		s.timeDurationSumGTTA -= e.Value.(int64)
	}
	s.avgGTTA = float32(s.timeDurationSumGTTA) / (float32(s.timeListGTTA.Len()) * 1000.0) // avgTime in seconds

	s.avgGTTALock.Unlock()
}

func (s *Spammer) InitPowClient() {
	cfg := config.AppConfig

	s.powSrvClient = &powsrvio.PowClient{APIKey: cfg.GetString("powSrvAPIKey"), ReadTimeOutMs: cfg.GetInt64("powSrvReadTimeoutInMs"), Verbose: false}
	err := s.powSrvClient.Init()
	for err != nil {
		logs.Log.Warningf("powSrvClient.Init() failed! Err: %v", err)
		time.Sleep(time.Duration(1) * time.Second)
		err = s.powSrvClient.Init()
	}
	s.powSrvClientErrSignal = make(chan struct{})
}

func (s *Spammer) StartSendThread() {
	cfg := config.AppConfig

	go func() {

		for {

			var txs []trinary.Trytes

			fnSendTx := func(apiNode *nodelist.ApiNode) error {
				_, err := apiNode.API.BroadcastTransactions(txs...)
				if err != nil {
					if strings.Contains(strings.ToLower(err.Error()), "invalid transaction hash") {
						return nil
					}
					if strings.Contains(strings.ToLower(err.Error()), "invalid trytes") {
						return nil
					}

					s.txsChannel <- txs // resend the txs
					return fmt.Errorf("BroadcastTransactions: %v", err.Error())
				}

				return nil
			}

			for len(txs) < cfg.GetInt("sendTxAtOnceCount") {
				// Collect the tx
				if s.quitSignal {
					return
				}

				b, hasMore := <-s.txsChannel
				if !hasMore {
					return
				}
				txs = append(txs, b...)

				s.AddAvgTPS(len(b))
			}

			if s.quitSignal {
				return
			}

			signalChanTimeout := make(chan bool, 1)
			err := s.nodeList.ExecuteFunc(signalChanTimeout, s.signalChannelQuit, fnSendTx, cfg.GetInt("apiNodeMaxRetries"))
			for err != nil {
				if s.quitSignal {
					return
				}

				logs.Log.Warningf("ExecuteFunc: %v", err.Error())
				err = s.nodeList.ExecuteFunc(signalChanTimeout, s.signalChannelQuit, fnSendTx, cfg.GetInt("apiNodeMaxRetries"))
			}
		}
	}()
}

func (s *Spammer) doPow(b bundle.Bundle, trunk trinary.Hash, branch trinary.Hash, mwm int) error {
	cfg := config.AppConfig

	var prev trinary.Hash

	for i := len(b) - 1; i >= 0; i-- {
		if s.quitSignal {
			return nil
		}

		switch {
		case i == len(b)-1:
			// Last tx in the bundle
			b[i].TrunkTransaction = trunk
			b[i].BranchTransaction = branch
		default:
			b[i].TrunkTransaction = prev
			b[i].BranchTransaction = trunk
		}

		b[i].AttachmentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
		b[i].AttachmentTimestampLowerBound = consts.LowerBoundAttachmentTimestamp
		b[i].AttachmentTimestampUpperBound = consts.UpperBoundAttachmentTimestamp

		trytes, err := transaction.TransactionToTrytes(&b[i])
		if err != nil {
			return nil
		}

		var nonce trinary.Trytes
		var durationPOW time.Duration

		if s.powFuncLocal != nil {
			// Local PoW
			s.powSemaphore <- 1 // channel is not full => semaphore aquire
			timePOW := time.Now()
			nonce, err = s.powFuncLocal(trytes, mwm)
			durationPOW = time.Since(timePOW)
			<-s.powSemaphore // read once from the channel => semaphore release

			if err != nil {
				return err
			}
		} else {
			// PoW via powsrv.io
			s.powSrvClientLock.RLock()

			select {
			case <-s.powSrvClientErrSignal: // powSrvClient got an Error in another go routine
				s.powSrvClientLock.RUnlock()
				return errors.New("powSrvClient got an error in another go routine")

			case <-s.signalChannelQuit: // spammer stopped
				s.powSrvClientLock.RUnlock()
				return nil

			case s.powSemaphore <- 1: // channel is not full => semaphore aquire
				timePOW := time.Now()
				nonce, err = s.powSrvClient.PowFunc(trytes, mwm)
				durationPOW = time.Since(timePOW)

				<-s.powSemaphore // read once from the channel => semaphore release
				s.powSrvClientLock.RUnlock()

				if err != nil {
					// Check again if there was an error in the meantime
					select {
					case <-s.powSrvClientErrSignal:
						return errors.New("powSrvClient got an error in another go routine")
					default:
						close(s.powSrvClientErrSignal) // send a signal to all other go routines
					}

					if s.quitSignal {
						return nil
					}

					// aquire all workers to close the connection in case of error
					s.powSrvClientLock.Lock()
					s.powSrvClient.Close()
					s.InitPowClient()
					s.powSrvClientLock.Unlock()

					return err
				}
			}
		}

		s.AddAvgPOW(1)

		if cfg.GetBool("printPowDuration") {
			logs.Log.Infof("PoW took %v", durationPOW.Truncate(time.Millisecond))
		}

		b[i].Nonce = nonce

		// set new transaction hash
		b[i].Hash = TransactionHash(&b[i])
		prev = b[i].Hash
	}
	return nil
}

func (s *Spammer) gracefullyDies() {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	sig := <-ch // waits for death signal
	logs.Log.Infof("Caught signal '%s': TangleKit Spammer is shutting down. Please wait...", sig)

	s.Close()
	logs.Log.Info("Bye!")
	deathWaitGroup.Done()
}

func init() {
	logs.Setup("spammer", "%{color}[%{level:.4s}] %{time:15:04:05.00} -> %{color:reset}%{message}")
	logs.SetLogLevel("spammer", "DEBUG")
}

func logo() {
	logs.Log.Info(`

████████╗ █████╗ ███╗   ██╗ ██████╗ ██╗     ███████╗██╗  ██╗██╗████████╗
╚══██╔══╝██╔══██╗████╗  ██║██╔════╝ ██║     ██╔════╝██║ ██╔╝██║╚══██╔══╝
   ██║   ███████║██╔██╗ ██║██║  ███╗██║     █████╗  █████╔╝ ██║   ██║
   ██║   ██╔══██║██║╚██╗██║██║   ██║██║     ██╔══╝  ██╔═██╗ ██║   ██║
   ██║   ██║  ██║██║ ╚████║╚██████╔╝███████╗███████╗██║  ██╗██║   ██║
   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝   ╚═╝

    ███████╗██████╗  █████╗ ███╗   ███╗███╗   ███╗███████╗██████╗
    ██╔════╝██╔══██╗██╔══██╗████╗ ████║████╗ ████║██╔════╝██╔══██╗
    ███████╗██████╔╝███████║██╔████╔██║██╔████╔██║█████╗  ██████╔╝
    ╚════██║██╔═══╝ ██╔══██║██║╚██╔╝██║██║╚██╔╝██║██╔══╝  ██╔══██╗
    ███████║██║     ██║  ██║██║ ╚═╝ ██║██║ ╚═╝ ██║███████╗██║  ██║
    ╚══════╝╚═╝     ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝

                 Welcome to TangleKit Spammer!`)
}

func main() {
	deathWaitGroup.Add(1)

	s := Spammer{}
	s.InitSpammer()

	logo()

	go s.RunSpammer()
	go s.gracefullyDies()

	deathWaitGroup.Wait()
}
