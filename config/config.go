package config

import (
	"encoding/json"
	"os"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gitlab.com/tanglekit/go/logs"
)

var (
	AppConfig = viper.New()
)

/*
PRECEDENCE (Higher number overrides the others):
1. default
2. key/value store
3. config
4. env
5. flag
6. explicit call to Set
*/
func LoadFlags() {
	flag.String("address", "TANGLEKIT99SPAMMER99USING99POWSRVIO9999999999999999999999999999999999999999999999", "Tx Address")
	flag.String("message", "Spamming with https://gitlab.com/tanglekit/spammer using https://powsrv.io", "Message of the Tx")
	flag.String("tag", "TANGLEKIT9SPAMMER9999999999", "Tag of the Tx")
	flag.Bool("addNodeAddressInMessage", false, "Add node address in Tx message")

	flag.Int("mwm", 14, "Minimum weight magnitude (14 for IOTA mainnet)")
	flag.Int64("depth", 3, "Depth of the random walker")

	flag.Bool("powLocal", false, "Use local PoW instead of powsrv.io")
	flag.Int64("powSrvReadTimeoutInMs", 10000, "Timeout for a request to powsrv.io")
	flag.Int("powWorkersCount", 100, "How much PoW can be done in parallel by powSrv")
	flag.Int("tipsBufferSize", 100, "How many tips should be buffered (ATTENTION: high values lead to lazy tips!)")
	flag.Int64("powTpsRateLimit", 0, "Rate limit for the spam (0 = no limit)")
	flag.String("powSrvAPIKey", "", "API Key for powsrv.io")

	flag.StringSlice("nodeList", []string{""}, "IOTA fullnodes to use for spamming")
	flag.StringSlice("validNodeVersions", []string{""}, "Valid IRI versions (leave empty for all versions)")
	flag.Int64("apiNodeTimeout", 60, "Timeout for IRI API requests")
	flag.Int("apiNodeMaxRetries", 5, "How often should IRI API requests be repeated in case of an error")
	flag.Int64("milestoneTolerance", 3, "Maximum distance from latest milestone index to accept node as synced")

	flag.Bool("printPowDuration", false, "Print duration of the POW requests")
	flag.Bool("printGTTADuration", false, "Print duration of the gTTA requests")

	flag.Int("sendTxAtOnceCount", 10, "How many tx should be send to a node at once")

	AppConfig.BindPFlags(flag.CommandLine)
	var configPath = flag.StringP("config", "c", "config.json", "Config file path")
	flag.Parse()

	loadAppConfigFile(configPath)

	cfg, _ := json.MarshalIndent(AppConfig.AllSettings(), "", "  ")
	logs.Log.Debugf("Settings loaded: \n %+v", string(cfg))
}

func loadAppConfigFile(configPath *string) {
	if len(*configPath) > 0 {
		_, err := os.Stat(*configPath)
		if !flag.CommandLine.Changed("config") && os.IsNotExist(err) {
			// Standard config file not found => skip
			logs.Log.Info("Standard config file not found. Loading default settings.")
		} else {
			logs.Log.Infof("Loading config from: %s", *configPath)
			AppConfig.SetConfigFile(*configPath)
			err := AppConfig.ReadInConfig()
			if err != nil {
				logs.Log.Fatalf("Config could not be loaded from: %s (%s)", *configPath, err)
			}
		}
	}
}
