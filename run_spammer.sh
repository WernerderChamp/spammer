#!/bin/bash

# Choose the fastest setting your CPU supports
go build -tags 'pow_c' spammer.go
#go build -tags 'pow_sse' spammer.go
#go build -tags 'pow_avx' spammer.go
if [ $? -ne 0 ]; then
    exit 1
fi

./spammer