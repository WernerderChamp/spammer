module gitlab.com/tanglekit/spammer

go 1.12

require (
	github.com/iotadevelopment/go v0.0.0-20190417025043-28d46cb4ce25
	github.com/iotaledger/iota.go v1.0.0-beta.7
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.4.0
	gitlab.com/powsrv.io/go/api v0.0.0-20180825222338-acd4e4dcc0bf // indirect
	gitlab.com/powsrv.io/go/client v0.0.0-20190717084126-be3e20edad49
	gitlab.com/tanglekit/go/logs v0.0.0-20190721161506-c7a10c1be208
	gitlab.com/tanglekit/go/nodelist v0.0.0-20190721161729-917e5c0d113f
)
